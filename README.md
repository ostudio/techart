# TechArt

## Unity version

The assignment made with unity 2021.3.14f1 LTS

## About the assignment

The assignment was made on the “TechArtScene” as demanded. 
the scene include 3 buttons:
- First button to show the popup. 
- Second button to show the UI elements.
- Threed to close the popup 
the buttons in the scene only use for controlling the scene.

## Note

- Hierarchy organized with empty gameobjects tagged as "Editor Only" to not be included in the build.
- Sprites are wrapped in atlas to reduce the "DrawCall" (can be checked with frame debugger).
- The assets from the "PSD" file was sliced under the idea of try to divide it the less as you can to maintain size optimization while keeping best graphics and extensive structure.
- All assets max size was picked as 1024 to save size even that some devices can support higher max size (can be changed easily with demand).  

## UnityPackeg

https://drive.google.com/file/d/1VjBucBx_XOFC6w1lN5ax81L13cq1ZmWC/view?usp=share_link
